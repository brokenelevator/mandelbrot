import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import static java.awt.image.BufferedImage.*;
import static java.awt.GridBagConstraints.*;
import static java.lang.Math.*;

class Mandelbrot extends Frame implements WindowListener, ActionListener, RegionChooser
{
Mandelbrot(String title)
	{
	super(title);
	}
public void windowClosing(WindowEvent e)
	{
		dispose();
		try
			{
			System.exit(0);
			}
		catch(SecurityException ex)
			{
			System.out.println("Nie mozna zamknac okna");
			}
	}
public void windowClosed(WindowEvent e)
	{
	}
public void windowIconified(WindowEvent e)
	{
	}
public void windowDeiconified(WindowEvent e)
	{
	}
public void windowOpened(WindowEvent e)
	{
	}
public void windowActivated(WindowEvent e)
	{
	}
public void windowDeactivated(WindowEvent e)
	{
	}
public void init()
	{
	setSize(780, 650);
	setVisible(true);
	addWindowListener(this);
	}
int biwidth = 600, biheight = 600;
double xunit, yunit, xorgin=0, yorgin=0, radius = 4;
static Mandelbrot f = new Mandelbrot("Mandelbrot set");
static ImagePanel ip = new ImagePanel();
BufferedImage bi = new BufferedImage(biwidth, biheight, TYPE_INT_RGB);
GridBagLayout gbl = new GridBagLayout();
GridBagConstraints gbc = new GridBagConstraints();
TextField tf1 = new TextField("-2+i", 5);
TextField tf2 = new TextField("1-i");
TextField tf3 = new TextField("4");
TextField tf4 = new TextField("600");
TextField tf5 = new TextField("600");
Complex pmin = new Complex("-2+i");
Complex pmax = new Complex("1-i");

public void actionPerformed(ActionEvent e)
	{
	Complex tmpmin;
	Complex tmpmax;
	String strtmp = new String();
	strtmp = tf4.getText();
	biwidth = new Integer(strtmp);
	strtmp = tf5.getText();
	biheight = new Integer(strtmp);
	strtmp = tf3.getText();
	radius = new Integer(strtmp);
	strtmp = tf1.getText();
	tmpmin = new Complex(strtmp);
	strtmp = tf2.getText();
	tmpmax = new Complex(strtmp);
	pmin.setRe(min(tmpmin.Re(), tmpmax.Re()));
	pmin.setIm(max(tmpmin.Im(), tmpmax.Im()));
	pmax.setRe(max(tmpmin.Re(), tmpmax.Re()));
	pmax.setIm(min(tmpmin.Im(), tmpmax.Im()));
	bi = new BufferedImage(biwidth, biheight, TYPE_INT_RGB);
	plotMandelbrot();
	}
public void makeLayout()	
	{
	Button b1 = new Button("PLOT");
	b1.addActionListener(this);
	Label l1 = new Label("p(min)", Label.CENTER);
	Label l2 = new Label("p(max)", Label.CENTER);
	Label l3 = new Label("r", Label.CENTER);
	Label l4 = new Label("Width", Label.CENTER);
	Label l5 = new Label("Height", Label.CENTER);
	gbc.insets = new Insets(125, 10, 25, 10);
	gbc.fill=BOTH;
	gbc.gridx = 1;
	gbc.gridy = 1;
	gbl.setConstraints(l1, gbc);
	gbc.gridx = 2;
	gbc.gridy = 1;
	gbl.setConstraints(tf1, gbc);
	gbc.insets = new Insets(10, 10, 25, 10);
	gbc.gridx = 1;
	gbc.gridy = 2;
	gbl.setConstraints(l2, gbc);
	gbc.gridx = 2;
	gbc.gridy = 2;
	gbl.setConstraints(tf2, gbc);
	gbc.gridx = 1;
	gbc.gridy = 3;
	gbl.setConstraints(l3, gbc);
	gbc.gridx = 2;
	gbc.gridy = 3;
	gbl.setConstraints(tf3, gbc);
	gbc.gridx = 1;
	gbc.gridy = 4;
	gbl.setConstraints(l4, gbc);
	gbc.gridx = 2;
	gbc.gridy = 4;
	gbl.setConstraints(tf4, gbc);
	gbc.gridx = 1;
	gbc.gridy = 5;
	gbl.setConstraints(l5, gbc);
	gbc.gridx = 2;
	gbc.gridy = 5;
	gbl.setConstraints(tf5, gbc);
	gbc.fill = HORIZONTAL;
	gbc.anchor = NORTH;
	gbc.gridx = 2;
	gbc.gridy = 6;
	gbl.setConstraints(b1, gbc);
	gbc.insets = new Insets(4, 4, 4, 4);
	gbc.anchor = CENTER;
	gbc.fill = BOTH;
	gbc.gridx = 3;
	gbc.gridy = 1;
	gbc.weightx = 1;
	gbc.weighty = 1;
	gbc.gridheight = 6;
	gbl.setConstraints(ip, gbc);
	add(l1);
	add(l2);
	add(l3);
	add(l4);
	add(l5);
	add(tf1);
	add(tf2);
	add(tf3);
	add(tf4);
	add(tf5);
	add(b1);
	ip.make_update(bi);
	add(ip);
	setLayout(gbl);
	doLayout();
	}
public void RegionChoosen(int ppx, int ppy, int dpx, int dpy)
	{
	xunit = (biwidth / (abs(pmin.Re())+abs(pmax.Re())));
	xorgin = -xunit*min(pmin.Re(), pmax.Re());
	yunit = (biheight / (abs(pmin.Im())+abs(pmax.Im())));
	yorgin = yunit*max(pmin.Im(), pmax.Im());
	pmin.setRe((ppx-xorgin)/xunit);
	pmin.setIm((ppy-yorgin)/(-yunit));
	pmax.setRe((dpx-xorgin)/xunit);
	pmax.setIm((dpy-yorgin)/(-yunit));
	Complex tmpmin = new Complex(pmin);
	Complex tmpmax = new Complex(pmax);;
	pmin.setRe(min(tmpmin.Re(), tmpmax.Re()));
	pmin.setIm(max(tmpmin.Im(), tmpmax.Im()));
	pmax.setRe(max(tmpmin.Re(), tmpmax.Re()));
	pmax.setIm(min(tmpmin.Im(), tmpmax.Im()));
	remove(tf1);
	remove(tf2);
	tf1 = new TextField(pmin.toString(), 5);
	tf2 = new TextField(pmax.toString(), 5);
	gbc = new GridBagConstraints();
	gbc.insets = new Insets(125, 10, 25, 10);
	gbc.gridx = 2;
	gbc.gridy = 1;
	gbl.setConstraints(tf1, gbc);
	gbc.insets = new Insets(10, 10, 25, 10);
	gbc.gridx = 2;
	gbc.gridy = 2;
	gbl.setConstraints(tf2, gbc);
	add(tf1);
	add(tf2);
	setLayout(gbl);
	doLayout();
	plotMandelbrot();
	}
public void dot_coordinate(Complex p, int i)
	{
	int rgb = 0;
	double x, y;
	x = (p.Re()*xunit)+xorgin;
	y = -(p.Im()*yunit)+yorgin;
	if(i == 1) rgb = 0xFF0000;
	else if(i == 11 || i == 21 || i == 31 || i == 41 || i == 51 || i == 61 || i == 71 || i == 81 || i == 91) rgb = 0xC5A03A;
	else if(i == 2) rgb = 0x00FF00;
	else if(i == 12 || i == 22 || i == 32 || i == 42 || i == 52 || i == 62 || i == 72 || i == 82 || i == 92)  rgb = 0x3AC586;
	else if(i == 3 || i == 13 || i == 23 || i == 33 || i == 43 || i == 53 || i == 63 || i == 73 || i == 83 || i == 93) rgb = 0x0000FF;
	else if(i == 4 || i == 14 || i == 24 || i == 34 || i == 44 || i == 54 || i == 64 || i == 74 || i == 84 || i == 94) rgb = 0xFF00FF;
	else if(i == 5 || i == 15 || i == 25 || i == 35 || i == 45 || i == 55 || i == 65 || i == 75 || i == 85 || i == 95) rgb = 0xFFFF00;
	else if(i == 6 || i == 16 || i == 26 || i == 36 || i == 46 || i == 56 || i == 66 || i == 76 || i == 86 || i == 96) rgb = 0xFF8080;
	else if(i == 7 || i == 17 || i == 27 || i == 37 || i == 47 || i == 57 || i == 67 || i == 77 || i == 87 || i == 97) rgb = 0x00FFFF;
	else if(i == 8 || i == 18 || i == 28 || i == 38 || i == 48 || i == 58 || i == 68 || i == 78 || i == 88 || i == 98) rgb = 0xFF8080;
	else if(i == 9 || i == 19 || i == 29 || i == 39 || i == 49 || i == 59 || i == 69 || i == 79 || i == 89 || i == 99) rgb = 0x80FF80;
	else if(i == 10 || i == 20 || i == 30 || i == 40 || i == 50 || i == 60 || i == 70 || i == 80 || i == 90) rgb = 0xFF0080;
	else if(i == 100) rgb = 0xFFFFFF; //kolor bialy
	//else if(i == 100) rgb = 0x000000; // kolor czarny
	if(i != 0) bi.setRGB((int)floor(x), (int)floor(y), rgb);
	}
public void plotMandelbrot()
	{
	xunit = (biwidth / (abs(pmin.Re())+abs(pmax.Re())));
	xorgin = -xunit*min(pmin.Re(), pmax.Re());
	yunit = (biheight / (abs(pmin.Im())+abs(pmax.Im())));
	yorgin = yunit*max(pmin.Im(), pmax.Im());
	Complex p = new Complex(min(pmin.Re(), pmax.Re()), max(pmin.Im(), pmin.Im()));
	Complex temp = new Complex();
	int i, j=0;
	while(j < biwidth)
		{
		int k=0;
		double pim =  max(pmin.Im(), pmin.Im());
		while(k < biheight)
			{
			if(p.Abs() < radius)
				{
				i=2;
				temp.setRe(p.Re());
				temp.setIm(p.Im());
				while(i < 100)
					{
					temp.Mul(temp).Add(p);
					if(temp.Abs() > radius)
						{
						i--;
						break;
						}
					i++;
					}
				} else i=0;
			dot_coordinate(p, i);
			k++;
			p.setIm(p.Im()-(1/yunit));
			}
		j++;
		p.setRe(p.Re()+(1/xunit));
		p.setIm(pim);
		}
	remove(ip);
	gbc.insets = new Insets(4, 4, 4, 4);
	gbc.anchor = CENTER;
	gbc.fill = BOTH;
	gbc.gridx = 3;
	gbc.gridy = 1;
	gbc.weightx = 1;
	gbc.weighty = 1;
	gbc.gridheight = 6;
	gbl.setConstraints(ip, gbc);
	ip.make_update(bi);
	add(ip);
	setLayout(gbl);
	doLayout();
	}
public static void main(String[] args)
	{
	ip.get_mandelbrot_object(f);
	f.init();
	f.makeLayout();
	f.plotMandelbrot();
	}
}