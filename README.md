# Mandelbrot
This is a project for Java Programming Workshop.

## Project overview
Mandelbrot is an application that plots a Mandelbrot set. Check out Wikipedia entry for [Mandelbrot set](https://en.wikipedia.org/wiki/Mandelbrot_set).   
I wrote it back in 2010. It uses AWT.   
One day I might refactor it.

## Screenshot
![Mandelbrot](screenshot.png)

## How to run it

### Compile
```bash
javac Mandelbrot.java
```

### Run
```bash
java Mandelbrot
```
