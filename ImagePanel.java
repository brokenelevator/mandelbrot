import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import static java.awt.image.BufferedImage.*;
import static java.awt.Color.*;

class ImagePanel extends Panel implements MouseMotionListener, MouseListener
{
Mandelbrot mdb;
BufferedImage b;
Point presspoint, olddragpoint, dragpoint;
ImagePanel()
	{
	addMouseMotionListener(this);
	addMouseListener(this);
	}
public void mouseDragged(MouseEvent e)
	{
	dragpoint = e.getPoint();
	Graphics g = getGraphics();
	g.setXORMode(BLUE);
	int x, y, w, h;
	if(olddragpoint.x > presspoint.x)
		{
		x=presspoint.x;
		w=olddragpoint.x-presspoint.x;
		} else 
			{
			x=olddragpoint.x;
			w=presspoint.x-olddragpoint.x;
			}
	if(olddragpoint.y > presspoint.y)
		{
		y=presspoint.y;
		h=olddragpoint.y-presspoint.y;
		} else
			{
			y=olddragpoint.y;
			h=presspoint.y-olddragpoint.y;
			}
	g.fillRect(x, y, w, h);
	if(dragpoint.x > presspoint.x) {x=presspoint.x; w=dragpoint.x-presspoint.x;}else {x=dragpoint.x; w=presspoint.x-dragpoint.x;}
	if(dragpoint.y > presspoint.y) {y=presspoint.y; h=dragpoint.y-presspoint.y;}else {y=dragpoint.y; h=presspoint.y-dragpoint.y;}
	g.fillRect(x, y, w, h);
	olddragpoint = dragpoint;
	}
public void mousePressed(MouseEvent e)
	{
	olddragpoint = e.getPoint();
	presspoint = olddragpoint;
	}
public void mouseReleased(MouseEvent e)
	{
	if(presspoint != dragpoint) mdb.RegionChoosen(presspoint.x, presspoint.y, dragpoint.x, dragpoint.y);
	}
public void mouseMoved(MouseEvent e)
	{
	}
public void mouseClicked(MouseEvent e)
	{
	}
public void mouseEntered(MouseEvent e)
	{
	}
public void mouseExited(MouseEvent e)
	{
	}
public void make_update(BufferedImage bis)
	{
	b = bis;
	}
public void get_mandelbrot_object(Mandelbrot bis)
	{
	mdb = bis;
	}
public void paint(Graphics g)
	{
	g.drawImage(b, 0, 0, null);
	}
}
