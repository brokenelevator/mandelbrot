import static java.lang.Math.*;
import static java.lang.Double.NaN;
import java.util.StringTokenizer;

public class Complex
{
private double re, im;
public Complex()
{
	re=0;
	im=0;
}
public Complex(double a)
{
	re=a;
	im=0;
}
public Complex(double a, double b)
{
	re=a;
	im=b;
}
public Complex(Complex a)
{
	re=a.re;
	im=a.im;
}
public Complex(String a)
{
	String tmp = new String();
	StringTokenizer st = new StringTokenizer(a, "+-i", true);
	tmp = st.nextToken();
	if(tmp.equals("-"))
		{
		String tmp2 = st.nextToken();
		if(tmp2.equals("i"))
			{
			re=0;
			im=-1;
			return;
			}
		Double wrapre = new Double(tmp + tmp2);
		re=wrapre;
		} else
	if(tmp.equals("+"))
		{
		Double wrapre = new Double(st.nextToken());
		re=wrapre;
		} else
	if(tmp.equals("i"))
		{
		re=0;
		im=1;
		return;
		}
	else
		{
		Double wrapre = new Double(tmp);
		re=wrapre;
		}
	if(st.hasMoreTokens())
		{
		tmp = st.nextToken();
		if(tmp.equals("i"))
			{
			im=re;
			re=0;
			}
		else
			{
			if(tmp.equals("-"))
				{
				String tmp2 = st.nextToken();
				if(tmp2.equals("i"))
					{
					im=-1;
					return;
					}
				Double wrapim = new Double(tmp + tmp2);
				im=wrapim;
				} else
			if(tmp.equals("+"))
				{
				String tmp2 = st.nextToken();
				if(tmp2.equals("i"))
					{
					im=1;
					return;
					}
				Double wrapim = new Double(tmp2);
				im=wrapim;
				}
			}
		}
		else im=0;
}
static public Complex Add(Complex add1, Complex add2)
{
	Complex wynik = new Complex(add1.re+add2.re, add1.im+add2.im);
	return wynik;
} 
static public Complex Sub(Complex sub1, Complex sub2)
{
	Complex wynik = new Complex(sub1.re-sub2.re, sub1.im-sub2.im);
	return wynik;
} 
static public Complex Mul(Complex mul1, Complex mul2)
{
	Complex wynik = new Complex((mul1.re*mul2.re)-(mul1.im*mul2.im), (mul1.im*mul2.re)+(mul1.re*mul2.im));
	return wynik;
} 
static public Complex Div(Complex div1, Complex div2)
{
	Complex wynik = new Complex(((div1.re*div2.re)+(div1.im*div2.im))/((pow(div2.re,2.))+(pow(div2.im,2.))), ((div1.im*div2.re)-(div1.re*div2.im))/((pow(div2.re,2.))+(pow(div2.im,2.))));
	return wynik;
} 
static public double Abs(Complex sa)
{
	double tosqrt;
	tosqrt = pow(sa.re,2.) + pow(sa.im,2.);
	return sqrt(tosqrt);
} 
static public double Phrase(Complex sa)
{
	if(sa.re > 0) return atan(sa.im/sa.re);
	if(sa.re < 0) return atan(sa.im/sa.re) + PI;
	if(sa.re == 0)
	{
		if(sa.im > 0) return (1/2) * PI;
		if(sa.im > 0) return (-(1/2)) * PI;
	}
return NaN;
}
static public double SqrAbs(Complex sa)
{
	Complex revsa = new Complex(sa.re, -sa.im);
	return ((sa.re*revsa.re)-(sa.im*revsa.im));
} 
static public double Re(Complex sa)
{
	return sa.re;
} 
static public double Im(Complex sa)
{
	return sa.im;
}
public Complex Add(Complex sa)
{
	this.re = this.re+sa.re;
	this.im = this.im+sa.im;
	return this;
}
public Complex Sub(Complex sa)
{
	this.re = this.re-sa.re;
	this.im = this.im-sa.im;
	return this;
}
public Complex Mul(Complex sa)
{
	double r = (this.re*sa.re)-(this.im*sa.im);
	this.im=(this.im*sa.re)+(this.re*sa.im);
	this.re=r;
	return this;
}
public Complex Div(Complex sa)
{
	double r = ((this.re*sa.re)+(this.im*sa.im))/((pow(sa.re,2.))+(pow(sa.im,2.)));
	this.im = ((this.im*sa.re)-(this.re*sa.im))/((pow(sa.re,2.))+(pow(sa.im,2.)));
	this.re = r;
	return this;
}
public double Abs()
{
	double tosqrt;
	tosqrt = pow(this.re,2.) + pow(this.im,2.);
	return sqrt(tosqrt);
}
public double SqrAbs()
{
	Complex revsa = new Complex(this.re, -this.im);
	return ((this.re*revsa.re)-(this.im*revsa.im));
}
public double Re()
{
	return this.re;
}
public double Im()
{
	return this.im;
}
public String toString()
{
	if(this.re != 0 && this.im > 0) return this.re + "+" + this.im + "i";
	if(this.re != 0 && this.im < 0) return this.re + "" + this.im + "i";
	if(this.re != 0 && this.im == 0) return this.re + " ";
	if(this.re == 0) return this.im + "i";
	return "0";
}
static public Complex valueOf(String str)
{
	Complex wynik = new Complex(str);
	return wynik;
}
public void setRe(double dbl)
{
	this.re=dbl;
}
public void setIm(double dbl)
{
	this.im=dbl;
}
public void setVal(Complex sa)
{
	this.re=sa.re;
	this.im=sa.im;
}
}
